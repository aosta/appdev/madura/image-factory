#!/bin/bash

set -eo pipefail
[[ "$TRACE" ]] && set -x

DistUpgrade () {
    export DEBIAN_FRONTEND=noninteractive
    # run update twice as it fails to update properly sometimes
    apt-get update -y
    apt-get update -y
    apt-get dist-upgrade -y
}

InstallMiddleWare() {
    export DEBIAN_FRONTEND=noninteractive
    apt-get install -y php7.2 php-zip php-xml php-mbstring php-curl php-xmlrpc php-soap php-gd php-cli php-bcmath php-tokenizer php-json php-pear composer apache2 libapache2-mod-php awscli jq
}

GetSiteCode() {
    chown -R ubuntu:ubuntu /var/www/html
    sudo -iu ubuntu curl -Lo- https://gitlab.com/aosta/appdev/madura/unsplash-search/-/archive/master/unsplash-search-master.tgz | sudo -iu ubuntu tar zxv -C /var/www/html --strip-components=2 unsplash-search-master/src/
}

FixSiteCodePerms() {
    chown -R ubuntu:www-data /var/www/html/storage
    chmod -R 0775 /var/www/html/storage
}

GenerateVhost() {
    tee /etc/apache2/sites-available/unsplash.conf <<EOF >/dev/null
<VirtualHost *:80>
    ServerName unsplash.aussielunix.io

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/public

    <Directory /var/www/html>
        AllowOverride All
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/unsplash-error.log
    CustomLog ${APACHE_LOG_DIR}/unsplash-access.log combined
</VirtualHost>
EOF
}

InstallSite() {
    su - ubuntu -c "cd /var/www/html/ && composer install"
}

FinaliseApache() {
    a2enmod rewrite
    a2dissite 000-default.conf
    a2ensite unsplash.conf
    systemctl stop apache2
}

main() {
    DistUpgrade
    InstallMiddleWare
    GetSiteCode
    FixSiteCodePerms
    GenerateVhost
    InstallSite
    FinaliseApache
}

main $@
